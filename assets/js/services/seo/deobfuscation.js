import eventEmitter, { EventsTypes } from "@Ui/js/services/events/eventEmitter";
import { requestAnimationFrame } from "@Ui/js/tools/requestAnimationFrame";

const OBFUSCATED_PREFIX = "js-b16";
const DICTIONARY = "0A12B34C56D78E9F";
export const DEOBFUSCATED_CLASSNAME = "xXx";

let obfuscatedLinks;
let obfuscatedSelectValue;

function decodeHref(url) {
  let ch = "";
  let cl = "";
  let d = "";
  for (let i = 0; i < url.length; i += 2) {
    ch = DICTIONARY.indexOf(url.charAt(i));
    cl = DICTIONARY.indexOf(url.charAt(i + 1));
    d += String.fromCharCode(ch * 16 + cl);
  }
  try {
    d = decodeURIComponent(d);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
  return d;
}

export function runAfterDeobfuscation(callback) {
  if (global.desobfuscationDone) {
    callback();
  } else {
    eventEmitter.once(EventsTypes.DESOBFUSCATION_DONE, callback);
  }
}

export function deobfuscateUrl(url) {
  if (!url) return "";

  // split for case with anchor
  const splitedUrl = url.split("#");
  let href = "";

  try {
    href = decodeHref(splitedUrl[0]);
  } catch (e) {
    console.error(e); // eslint-disable-line
  }

  if (typeof splitedUrl[1] !== "undefined") {
    href += "#" + splitedUrl[1];
  }
  return href;
}

function _deobfuscate(attribute = "href") {
  try {
    const attr = attribute === "value" ? "value" : "class";
    const info = this.getAttribute(attr).split(" ");

    const obfuscatedUrl = info[1];
    const href = deobfuscateUrl(obfuscatedUrl);

    switch (attribute) {
      case "value":
        this.setAttribute("value", href);
        break;
      default:
        this.setAttribute("href", href);
        break;
    }

    info[0] = DEOBFUSCATED_CLASSNAME;
    info[1] = null;
    this.setAttribute("class", info.join(" ").trim());

    // if item not a option tag we have to transform the span générated into a link
    if (attribute !== "value") {
      const objAttributes = Array.prototype.slice.call(this.attributes);
      const link = document.createElement("a");

      while (this.childNodes.length) {
        link.appendChild(this.firstChild);
      }

      objAttributes.forEach((item) => {
        link.setAttribute(item.name, item.value);
      });
      this.parentNode.insertBefore(link, this);
      this.parentNode.removeChild(this);
    }
  } catch (e) {
    /* eslint-disable no-console */
    console.error("desobfuscation error", e);
    /* eslint-enable no-console */
  }
}

function deobfuscateAll() {
  for (const link of obfuscatedLinks) {
    _deobfuscate.call(link, "href");
  }
  for (const link of obfuscatedSelectValue) {
    _deobfuscate.call(link, "value");
  }

  global.desobfuscationDone = true;
  eventEmitter.emit(EventsTypes.DESOBFUSCATION_DONE);
}

function _init() {
  global.desobfuscationDone = false;
  requestAnimationFrame(() => {
    obfuscatedLinks = document.querySelectorAll(
      `span[class^="${OBFUSCATED_PREFIX}"]`
    );
    obfuscatedSelectValue = document.querySelectorAll(
      `option[value^="${OBFUSCATED_PREFIX}"]`
    );
    deobfuscateAll();
  });
}

export default _init;
