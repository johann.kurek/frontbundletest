import { EventEmitter } from "events";

export const EventsTypes = {
  DESOBFUSCATION_DONE: "custom-events/desobfuscation-done",
  MQ_STATE: "custom-events/mq-change",
};

const eventEmitter = new EventEmitter();

export default eventEmitter;
