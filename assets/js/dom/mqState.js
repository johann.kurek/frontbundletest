import eventEmitter, { EventsTypes } from "@Ui/js/services/events/eventEmitter";
import { on } from "@Ui/js/dom/events";
import debounce from "@Ui/js/tools/debounce";

const mqIndicator = document.getElementById("mq-state");
const MQ_UNKNOWN = "unknown";

let currentState = MQ_UNKNOWN;

export const getState = () => currentState;

// The state may not return a number, which becomes
// problematic when you have to compare with other
// numbers. This function will return a number anyway.
export const getStateIndex = () =>
  window.Number.isInteger(currentState) ? currentState : 0;

const computeState = () => {
  if (!mqIndicator) {
    return false;
  }

  const index = parseInt(
    window.getComputedStyle(mqIndicator).getPropertyValue("z-index"),
    10
  );
  return index || MQ_UNKNOWN;
};

const initMqState = () => {
  currentState = computeState(); // on start, store the current state

  on(
    window,
    "resize",
    debounce(function resizeCallback() {
      const newState = computeState();
      if (currentState !== newState) {
        currentState = newState; // store the current state
        eventEmitter.emit(EventsTypes.MQ_STATE, currentState);
      }
    })
  );

  // media queries state detection
  window.MqState = {
    getState: getState,
  };
};

export default initMqState;
