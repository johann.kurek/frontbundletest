/**
 * description: Multiples methods for class manipulation
 * source: http://toddmotto.com/apollo-js-standalone-class-manipulation-api-for-html5-and-legacy-dom/
 * */

/**
 * Check if an element has a class
 * @param  {DomElement}  element   The element to check
 * @param  {string}  className the class to check
 * @return {Boolean}           true if the element has the class
 */
export function hasClass(element, className) {
  if (!element) return false;

  return element.classList.contains(className);
}

/**
 * Add a class to an element
 * @param {DomElement} element   The element to update
 * @param {String} className The class to add
 *
 * @return {void}
 */
export function addClass(element, className) {
  if (!element || hasClass(element, className)) {
    return false;
  }

  element.classList.add(className);
}

/**
 * Remove a class from an element
 * @param {DomElement} element   The element to update
 * @param {String} className The remove to remove
 *
 * @return {void}
 */
export function removeClass(element, className) {
  if (!element || !hasClass(element, className)) {
    return false;
  }

  element.classList.remove(className);
}

/**
 * Toggle a class from an element
 * @param {DomElement} element   The element to update
 * @param {String} className The class to toggle
 *
 * @return {void}
 */
export function toggleClass(element, className) {
  if (!element) return false;

  return element.classList.toggle(className);
}

/**
 * replace a class by another from an element
 * @param {DomElement} element   The element to update
 * @param {String} oldClassName class to remove
 * @param {String} newClassName class to add
 *
 * @return {void}
 */
export function replaceClass(element, oldClassName, newClassName) {
  if (!element) return false;

  return element.classList.replace(oldClassName, newClassName);
}
