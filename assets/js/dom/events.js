/**
 * Add a listener to a dom element
 * @param  {EventTarget}  element   The DOM element that will receive the event
 * @param  {String}       name      The event name
 * @param  {Function}     callback  Callback function
 * @param  {boolean}      capture   Initiate event capture (default false)
 * @return {void}
 */
export const on = (element, name, callback, capture = false) => {
  if (element) {
    element.addEventListener(name, callback, capture);
  }
};

/**
 * Remove a listener
 * @param  {EventTarget}  element   The DOM element that will receive the event
 * @param  {String}       name      The event name
 * @param  {Function}     callback  Callback function
 * @param  {boolean}      capture   Initiate event capture (default false)
 * @return {void}
 */
export const off = (element, name, callback, capture = false) => {
  if (element) {
    element.removeEventListener(name, callback, capture);
  }
};

/**
 * Add an event that will fire only once
 * @param  {EventTarget}  element   The DOM element that will receive the event
 * @param  {String}       name      The event name
 * @param  {Function}     callback  Callback function
 * @param  {boolean}      capture   Initiate event capture (default false)
 * @return {void}
 */
export const once = (element, name, callback, capture = false) => {
  const tmp = (ev) => {
    off(element, name, tmp, capture);
    callback.call(element, ev);
  };
  on(element, name, tmp, capture);
};

/**
 * Wait for page load
 *
 * @return {Promise} a promise that resolves when page is loaded
 */
export const deferToLoad = () => {
  return new Promise((resolve) => {
    if (document.readyState === "complete") {
      resolve(undefined);
    } else {
      once(window, "load", () => {
        resolve(undefined);
      });
    }
  });
};
