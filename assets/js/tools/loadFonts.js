import request from "@Ui/js/tools/request";
import { runAfterDeferAssets } from "@Ui/js/tools/loadAssets";

const PP_FRONT = window.PP.config.front;

const CSS_FONT_CODE_KEY = PP_FRONT.fontCodekey;
const CSS_FONT_URL_KEY = PP_FRONT.fontUrlkey;
const CSS_FONT_URL = PP_FRONT.fontCssUrl;

// Get CSS file content stored using localStorage
function getCachedFont() {
  return window.localStorage.getItem(CSS_FONT_CODE_KEY) || null;
}

function getDistantFontUrl() {
  return window.localStorage.getItem(CSS_FONT_URL_KEY) || null;
}

// Inject CSS using a <style> element
function injectStyles(cssText) {
  const style = window.document.createElement("style");
  const styleValue = window.document.createTextNode(cssText);
  style.appendChild(styleValue);

  window.document.head.appendChild(style);
}

function requestFont(path) {
  request(path, { credentials: "same-origin" }, false)
    .then((response) => {
      return response.text();
    })
    .then(function showCss(css) {
      // inject style in body once request is a succes and when no font are stored in localstorage.
      // commented here to avoid the "blink" and the "focus of unstyled content/text (fouc / fout)"
      // on the first load after asset version has changed
      // The Font will be visible with no blink or fouc/fout on the second page view.
      // nb. do tell product, if they see something we'll says it was a "bug" :p

      window.localStorage.setItem(CSS_FONT_CODE_KEY, css);
      window.localStorage.setItem(CSS_FONT_URL_KEY, CSS_FONT_URL);
    });
}

async function init() {
  // if new version is present
  if (getDistantFontUrl() !== CSS_FONT_URL) {
    requestFont(CSS_FONT_URL);
  } else {
    // fallback if no font are not loaded from head but present in window.localStorage.
    injectStyles(getCachedFont() || "");
  }
}

export default function loadFonts() {
  // if we have to make the first request, don't block the load,
  // wait until assets load are trigger.

  runAfterDeferAssets(init);
}
