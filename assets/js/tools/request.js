/**
 * Converts a payload to a readable format for fetch :
 *   - FormData object if available
 *   - string encoded like jquery.param : bar=foo&baz=1 ...
 * @param  {Object} parameters - the request parameters to convert
 * @return {FormData|String} - the payload body
 */
function getRequestBody(parameters) {
  if (
    parameters.headers &&
    parameters.headers["Content-Type"] === "application/json"
  ) {
    return JSON.stringify(parameters.payload);
  }

  const formData = new FormData();

  Object.keys(parameters.payload).forEach((element) => {
    formData.append(element, parameters.payload[element]);
  });
}

function needsBody(requestParameters) {
  return (
    requestParameters.payload &&
    (requestParameters.method === "POST" ||
      requestParameters.method === "PATCH")
  );
}

/**
 * Makes a request using Fetch (polyfilled if needed)
 * @param  {String} ressourceUrl      url of the distant ressource
 * @param  {Object} requestParameters Requests parameters
 * @param  {Boolean} json Set to false to avoid a JSON.parse of the response
 * @return {Promise}                  A promise representing request
 */
export default function request(
  ressourceUrl,
  requestParameters = {},
  json = true
) {
  const fetchParameters = Object.assign({}, requestParameters);
  if (needsBody(requestParameters)) {
    fetchParameters.body = getRequestBody(requestParameters);
  }
  return fetch(ressourceUrl, fetchParameters)
    .then((response) => {
      if (response.status >= 200 && response.status < 300) {
        return response;
      }
      const error = new Error(response.statusText);
      const contentType = response.headers.get("Content-Type");
      // handle errors responses containing a json body
      if (contentType.indexOf("application/json") !== -1) {
        return response.json().then((data) => {
          error.data = data;
          throw error;
        });
      }
      error.data = {};
      error.response = response;
      throw error;
    })
    .then((response) => {
      if (json && response.status !== 204) {
        return response.json();
      }
      return response;
    });
}
