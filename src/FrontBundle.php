<?php
namespace Webedia\FrontBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * FrontBundle
 */
class FrontBundle extends Bundle
{
    public function getPath(): string
    {
        return __DIR__;
    }
}